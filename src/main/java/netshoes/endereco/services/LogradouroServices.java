package netshoes.endereco.services;

import netshoes.endereco.dao.CepNaoEncontrado;
import netshoes.endereco.entidade.Logradouro;
import netshoes.endereco.validacao.CepInvalidoException;

public interface LogradouroServices {
	
	public Logradouro buscarLogradouroPorCep(String cep) throws CepInvalidoException, CepNaoEncontrado;
	
}

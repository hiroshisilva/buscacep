package netshoes.endereco.entidade;

public class Bairro {
	
	private Municipio municipio;
	
	private String nome;
	
	
	public Bairro() {
		super();
	}

	public Bairro(Municipio municipio, String nome) {
		super();
		this.municipio = municipio;
		this.nome = nome;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}

package netshoes;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class BuscaCepApplication {
	
    public static void main(String[] args) {
    	
        ConfigurableApplicationContext ctx = SpringApplication.run(BuscaCepApplication.class, args);
         
                String[] beanDefinitionNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanDefinitionNames);
        for (String beanName : beanDefinitionNames) {
            System.out.println(beanName);
        }
    }
}

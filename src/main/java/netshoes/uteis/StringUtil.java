package netshoes.uteis;

import java.text.Normalizer;

public class StringUtil {
	
	public static String getNumeros(String string){
		string = Normalizer.normalize(string, Normalizer.Form.NFD);
		string = string.replaceAll("[^0-9]", "");
		
		return string;
	}
	
}

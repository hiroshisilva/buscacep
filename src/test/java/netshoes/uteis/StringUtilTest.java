package netshoes.uteis;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilTest {

	@Test
	public void testGetNumeros() {
		String stringTest = "456789/*456";
		
		String stFinal = StringUtil.getNumeros(stringTest);
	
		Assert.assertThat(stFinal, is("456789456"));
	}
	
	@Test
	public void testGetNumerosRemoveBarra() {
		String stringTest = "456789-456";
		
		String stFinal = StringUtil.getNumeros(stringTest);
	
		Assert.assertThat(stFinal, is("456789456"));
	}
	
}

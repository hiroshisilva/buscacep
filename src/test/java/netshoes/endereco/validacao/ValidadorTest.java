package netshoes.endereco.validacao;

import org.junit.Test;

public class ValidadorTest {

	@Test(expected=CepInvalidoException.class)
	public void testValidarCEPError() throws CepInvalidoException {
		Validador.validarCEP("123456789");
	}
	
	
	@Test
	public void testValidarCEPOK() throws CepInvalidoException {
		Validador.validarCEP("12345678");
	}
	
}

package netshoes.endereco.dao;

import netshoes.endereco.entidade.Bairro;
import netshoes.endereco.entidade.Estado;
import netshoes.endereco.entidade.Logradouro;
import netshoes.endereco.entidade.Municipio;
import netshoes.endereco.entidade.Pais;

import org.springframework.stereotype.Component;

@Component
public class LogradouroDAOMock implements LogradouroDAO {

	@Override
	public Logradouro getLogradouroPorCEP(String cep)
			throws CepNaoEncontrado {

		if (cep.equals("08890123")) {
			throw new CepNaoEncontrado();
		
		} else if (cep.equals("08890120")) {
			throw new CepNaoEncontrado();
		
		}else if (cep.equals("08890100")) {
			throw new CepNaoEncontrado();
			
		}else if (cep.equals("08890000")) {
			throw new CepNaoEncontrado();
			
		}

		Pais pais = new Pais("Brasil", "1234");
		Estado estado = new Estado("São Paulo", pais, "123");
		Municipio municipio = new Municipio("São Paulo", "1235", estado);
		Bairro bairro = new Bairro(municipio, "Bela Vista");
		Logradouro logradouro = new Logradouro("Av Paulista", bairro);
		
		return logradouro;
	}

}

package netshoes.messages;

public enum Mensagens {
	
	CEP_INVALIDO("001","O cep informado é invalido"),
	CEP_NAO_ENCONTRADO("002", "O cep informado não foi encontrado.");
	
	private String codigo;
	private String mensagem;

	private Mensagens(String codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	
	
}

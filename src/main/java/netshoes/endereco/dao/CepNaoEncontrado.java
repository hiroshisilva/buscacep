package netshoes.endereco.dao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import netshoes.exception.BaseException;
import netshoes.messages.Mensagens;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="CEP_NAO_ECONTRADO")
public class CepNaoEncontrado extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8595631289780965552L;

	public CepNaoEncontrado() {
		super(Mensagens.CEP_NAO_ENCONTRADO);
	}
	
}

package netshoes.endereco.controllers;

import netshoes.endereco.dao.CepNaoEncontrado;
import netshoes.endereco.entidade.Logradouro;
import netshoes.endereco.services.LogradouroServices;
import netshoes.endereco.validacao.CepInvalidoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CepController {
	
	private LogradouroServices logradouroServices;
	
	@Autowired
	public CepController(LogradouroServices logradouroServices) {
		this.logradouroServices = logradouroServices;
	}

	@RequestMapping("/buscarCep/{cep}")
	public @ResponseBody Logradouro buscarCep(@PathVariable String cep) throws CepInvalidoException, CepNaoEncontrado{
		return logradouroServices.buscarLogradouroPorCep(cep);
	}

}

package netshoes.endereco.entidade;

public class Estado {
	
	private String nome;
	
	private Pais pais;
	
	private String codigoIBGE;

	
	public Estado() {
		super();
	}

	public Estado(String nome, Pais pais, String codigoIBGE) {
		super();
		this.nome = nome;
		this.pais = pais;
		this.codigoIBGE = codigoIBGE;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getCodigoIBGE() {
		return codigoIBGE;
	}

	public void setCodigoIBGE(String codigoIBGE) {
		this.codigoIBGE = codigoIBGE;
	}
	
}

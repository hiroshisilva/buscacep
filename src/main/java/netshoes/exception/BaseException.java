package netshoes.exception;

import netshoes.messages.Mensagens;

public abstract class BaseException extends Exception {

	private Mensagens msg;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -423787942154061339L;
	
	public BaseException(Mensagens mensagen) {
		super(mensagen.getCodigo() + " - "+ mensagen.getMensagem());
	}
	
	public Mensagens getErro(){
		return msg;
	}

}

package netshoes.endereco.entidade;

public class Municipio {
	
	private String nome;
	
	private String codigoIBGE;
	
	private Estado estado;
	
	public Municipio() {
		super();
	}

	public Municipio(String nome, String codigoIBGE, Estado estado) {
		super();
		this.nome = nome;
		this.codigoIBGE = codigoIBGE;
		this.estado = estado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigoIBGE() {
		return codigoIBGE;
	}

	public void setCodigoIBGE(String codigoIBGE) {
		this.codigoIBGE = codigoIBGE;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
}

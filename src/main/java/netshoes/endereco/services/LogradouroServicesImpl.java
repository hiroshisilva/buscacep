package netshoes.endereco.services;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import netshoes.endereco.dao.CepNaoEncontrado;
import netshoes.endereco.dao.LogradouroDAO;
import netshoes.endereco.entidade.Logradouro;
import netshoes.endereco.validacao.CepInvalidoException;
import netshoes.endereco.validacao.Validador;

@Component
public class LogradouroServicesImpl implements LogradouroServices {

	private LogradouroDAO logradouroDAO;

	@Autowired
	public LogradouroServicesImpl(LogradouroDAO logradouroDAO) {
		this.logradouroDAO = logradouroDAO;
	}

	@Override
	public Logradouro buscarLogradouroPorCep(String cep) throws CepInvalidoException, CepNaoEncontrado {
		
		Validador.validarCEP(cep);
		
		int i = 0;
		
		try{
			return logradouroDAO.getLogradouroPorCEP(cep);
		
		}catch(CepNaoEncontrado ex){
			
			if(i<3 && ! cep.substring(cep.length() -4, cep.length() -1).equals("000")){
				i++;
				
				cep = cep.substring(0, cep.length() -1 - i);
								
				cep = StringUtils.rightPad(cep, 8,"0");
				
				return logradouroDAO.getLogradouroPorCEP(cep);
				
			}else{
				throw new CepNaoEncontrado();
			}
		}
		
	}
}

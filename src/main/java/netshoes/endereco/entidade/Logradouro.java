package netshoes.endereco.entidade;

public class Logradouro {

	private String logradouro;
	
	private Bairro bairro;
	
	public Logradouro() {
		super();
	}

	public Logradouro(String logradouro, Bairro bairro) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
	
	
}

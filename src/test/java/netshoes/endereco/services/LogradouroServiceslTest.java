package netshoes.endereco.services;

import netshoes.endereco.dao.CepNaoEncontrado;
import netshoes.endereco.dao.LogradouroDAO;
import netshoes.endereco.dao.LogradouroDAOMock;
import netshoes.endereco.entidade.Logradouro;
import netshoes.endereco.validacao.CepInvalidoException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;

public class LogradouroServiceslTest {
	
	
	private LogradouroDAO logradouroDAO;
	private LogradouroServices services;
	
	@Before
	public void setUp() throws Exception {
		logradouroDAO = new LogradouroDAOMock();
		services = new LogradouroServicesImpl(logradouroDAO);
	}

	@Test(expected=CepNaoEncontrado.class)
	public void testBuscarLogradouroPorCepNaoEncontrado() throws CepInvalidoException, CepNaoEncontrado {
		services.buscarLogradouroPorCep("08890000");
	}
	
	@Test
	public void testBuscarLogradouroPorCepOk() throws CepInvalidoException, CepNaoEncontrado {
		Logradouro logradouro = services.buscarLogradouroPorCep("08891200");
		
		Assert.notNull(logradouro);
	}
	
	@Test(expected=CepInvalidoException.class)
	public void testBuscarLogradouroPorCepInvalido() throws CepInvalidoException, CepNaoEncontrado {
		services.buscarLogradouroPorCep("088912-000000");
		
	}

}

package netshoes.endereco.entidade;

public class Pais {

	private String nome;
	
	private String codigoIBGE;
	
	public Pais() {
		super();
	}

	public Pais(String nome, String codigoIBGE) {
		super();
		this.nome = nome;
		this.codigoIBGE = codigoIBGE;
	}

	public String getCodigoIBGE() {
		return codigoIBGE;
	}

	public void setCodigoIBGE(String codigoIBGE) {
		this.codigoIBGE = codigoIBGE;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}

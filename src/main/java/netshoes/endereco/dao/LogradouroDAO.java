package netshoes.endereco.dao;

import netshoes.endereco.entidade.Logradouro;

public interface LogradouroDAO {
	
	public Logradouro getLogradouroPorCEP(String cep) throws CepNaoEncontrado;
	
}

package netshoes.endereco.validacao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import netshoes.exception.BaseException;
import netshoes.messages.Mensagens;


@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="CEP_INVALIDO")
public class CepInvalidoException extends BaseException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1013001507066361712L;

	public CepInvalidoException() {
		super(Mensagens.CEP_INVALIDO); 
	}
}

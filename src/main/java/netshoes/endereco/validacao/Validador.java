package netshoes.endereco.validacao;

import netshoes.uteis.StringUtil;

public class Validador {
	
	public static void validarCEP(String cep) throws CepInvalidoException
	{
		cep = StringUtil.getNumeros(cep);
	
		if(cep.length()>8)
			throw new CepInvalidoException();
	}
	
}
